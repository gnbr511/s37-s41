const express = require('express')
const router = express.Router()
const CourseController = require('../controllers/CourseController')
const auth = require('../auth')

//Create single course
router.post('/create', auth.verify, (request, response) => {
	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	CourseController.addCourse(data).then((result) => {
		response.send(result)
	})
})

//Retrieve all courses
router.get('/', (request, response) => {
	CourseController.getAllCourses().then((result) => {
		response.send(result)
	})
})

//get active courses
router.get('/active', (request, response) => {
	CourseController.getAllActive().then((result) => {
		response.send(result)
	})
})

//get single course
router.get('/:courseId', (request, response) => {
	CourseController.getCourse(request.params.courseId).then((result) => response.send(result))
})

//update single course
router.patch('/:courseId/update', auth.verify, (request, response) => {
	CourseController.updateCourse(request.params.courseId, request.body).then((result) => response.send(result))
})

//archive course
router.patch('/:course_id/archive', auth.verify, (request, response) => {
	const data = {
		courseId: request.params.course_id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	CourseController.disableCourse(data).then((result) => response.send(result))
})

module.exports = router