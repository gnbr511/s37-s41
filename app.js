const express = require('express')
const mongoose =require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

dotenv.config()

const app = express()
const port = 8001

//MONGODB CONNECTION
mongoose.connect(`mongodb+srv://gnbr0511:${process.env.MONGODB_PASSWORD}@cluster0.h8bn8fo.mongodb.net/booking-system-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.once('open', () => console.log('Connected to MongoDB!'))
//MONGODB CONNECTION END

//TO AVOID CORS ERRORS WHEN TRYING TO SEND REQUEST TO OUR SERVER
app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended: true}))

//ROUTES
app.use('/users', userRoutes)

app.use('/courses', courseRoutes)
//ROUTES END

app.listen(port, () => {
	console.log(`API is now running on localhost:${port}`)
})