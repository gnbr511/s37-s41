const Course = require('../models/Course')

module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let new_course = new Course({
		name: data.course.name,
		description: data.course.description,
		price: data.course.price
		})
		return new_course.save().then((new_course, error) => {
			if(error){
				return false
			}
			return {
				message: 'New course successfully created!'
			}
		})
	}
	return Promise.resolve({message: 'User must be admin to access this.'})
}

module.exports.getAllCourses = () => {
	return Course.find({}).then((result) => {
		return result
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then((result) => result)
}

module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then((result) => result)
}

module.exports.updateCourse = (courseId, newData) => {
	return Course.findByIdAndUpdate(courseId, {
		name: newData.name,
		description: newData.description,
		price: newData.price,
		isActive: newData.isActive
	}).then((updatedCourse, error) => {
		if(error){
			false
		}
		return {
			message: 'Course has been updated successfully!'
		}
	})
}

module.exports.disableCourse = (data) => {
	if(data.isAdmin){
		return Course.findById(data.courseId).then((foundCourse, error) =>{
			if(error){
				return error
			}
			foundCourse.isActive = false
			return foundCourse.save().then((archivedCourse, error) => {
				if(error){
					return error
				}
				return {message: 'The course has been archive successfully!'}
			})
		})
	}
	return Promise.resolve({message: 'The user must be an admin to do this!'})
}